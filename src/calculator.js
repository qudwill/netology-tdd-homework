exports.add = (str) => {
	let splitter = /[,\n]/i;

	if (/^\/\/<.>\n/.test(str)) {
		splitter = str[3];
		str = str.slice(6);
	} else if (/(,\n)|(\n,)/.test(str)) {
		throw new Error('not valid input');
	}

	const numbers = str.split(splitter);
	let negativeNumbers = '';

	numbers.map(num => (parseInt(num) < 0) ? negativeNumbers += ` ${num}` : '');

	if (negativeNumbers != '') {
		throw new Error(`Отрицательные числа недопустимы${negativeNumbers}`);
	}

	return numbers.reduce((result, number) => result + (parseInt(number) || 0), 0);
}